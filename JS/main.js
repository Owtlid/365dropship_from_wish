import * as request from "./requests.js";
import * as productsGenerator from "./products.js";
import * as categoriesGenerator from "./categories.js";
//variables
const main = document.getElementById("main");
const categorySelect = document.getElementById("categories");
const sortType = document.getElementById("sort");
const searchButton = document.getElementById("buttonS");
const searchQuery = document.getElementById("inputS");
const resetButton = document.getElementById("reset");
const productQuantity = document.getElementsByClassName("header__counter")[0];
let productCounter = 0;
//getCategoriesOnLoad
const categoryList = await request.getCategories();
categorySelect.innerHTML = categoriesGenerator.categoriesHTMLGenerator(categoryList);
//getProductsOnLoad
const getProductsOnLoad = async (sortType = null) => {
  const listOfProducts = await request.getProducts(sortType);
  productCounter = listOfProducts.length;
  productQuantity.innerHTML = `selected 0 out of ${productCounter} products`;
  main.innerHTML = productsGenerator.productsHTMLGenerator(listOfProducts);
};
getProductsOnLoad();
//getSpecificCategory
categorySelect.addEventListener("change", () => {
  request.getSpecificCategory(categorySelect.value).then((response) => {
    main.innerHTML = productsGenerator.productsHTMLGenerator(response);
  });
});
//search
searchButton.addEventListener("click", () => {
  request.getProducts().then((response) => {
    const seekedList = response.filter(
      (item) => {
        item.title.toLowerCase().indexOf(searchQuery.value.toLowerCase()) !== -1
      });
    if (seekedList.length > 0) {
      main.innerHTML = productsGenerator.productsHTMLGenerator(seekedList);
    }
    if (seekedList.length === 0) {
      main.innerHTML = productsGenerator.errorHTMLGenerator();
    }
  });
});
//resetMainContent
resetButton.addEventListener("click", () => {
  getProductsOnLoad();
});
//sort
sortType.addEventListener("change", () => {
  getProductsOnLoad(sortType.value);
});
