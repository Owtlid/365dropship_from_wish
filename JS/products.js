const productsGenerator = (product) => `
<div class="catalog__product">
<div class="catalog__selector">
<input type="checkbox">
<button class="header__button catalog__button">Add To Inventory</button>
</div>
<div class="catalog__photo">
    <img src="${product.image}" alt="product photo">
</div>
<div class="catalog__title">
    ${product.title}
</div>
<div class="catalog__price">
    ${product.price}$
</div>
</div>
`;
const errorGenerator = `
<div class="catalog__error">   
</div>
`;
const productsHTMLGenerator = (productList) => {
  let str = "";
  for (const product of productList) {
    str += productsGenerator(product);
  }
  return str;
};
const errorHTMLGenerator = () => {
  return errorGenerator;
};
export { productsHTMLGenerator, errorHTMLGenerator };
