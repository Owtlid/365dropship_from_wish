import { serverAddress } from "./config.js";
const getData = async (url) => {
  try {
    const request = await fetch(serverAddress + url);
    return await request.json();
  } catch (err) {
    alert(err);
  }
};
const getCategories = async () => await getData("products/categories");
const getSpecificCategory = async (category) => await getData(`products/category/${category}`);
const getProducts = async (sortType = null) => await getData(`products${sortType ? `?sort=${sortType}` : ""}`);
export { getProducts, getCategories, getSpecificCategory };
