const categoriesGenerator = (category) => `
<option value="${category}">${category}</option>
`;
const categoriesHTMLGenerator = (categoryList) => {
  let str = "";
  for (const category of categoryList) {
    str += categoriesGenerator(category);
  }
  return str;
};
export { categoriesHTMLGenerator };
